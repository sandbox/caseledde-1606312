## Features 
This module provides a similar function as the Insert module (http://drupal.org/project/insert), only for any entity type.

The module that uses a JSON token as the Media module (http://drupal.org/project/media) does:

[ [{"type":"media","view_mode":"media_large","fid":"4","attributes":{"alt":"","class":"media-image","height":"360","width":"480"}}] ]

We believe that this is the best way to add field collections into the content of a node.

The use case is that you might want to add something like a table, HTML structure to your filterable content. You could use HTML directly or a WYSIWYG editor. But there is no abstraction or collection of your data. So you add a field collection or maybe a node/entity reference. But how do you get this into your text?

Therefore we try to provide a token on a common base that loads an entity and renders it with a given display and maybe some settings. This is a little step to an asset management so you can put any data in any entity and reuse it in any filtered text field.

http://groups.drupal.org/node/232348

## Download and installation
Download the sandbox project from http://drupal.org/sandbox/Caseledde/1606312

## Related projects
* Insert Field Collection (https://drupal.org/node/1887210)
* Insert (http://drupal.org/project/insert)
* Insert Block (http://drupal.org/project/insert_block)
* Token Insert (http://drupal.org/project/token_insert)
* Insert View (http://drupal.org/project/insert_view)